#pragma once
#include <string>

using namespace std;

class Animals
{
private:
	string name;
public:
	Animals() {};
	Animals(string name) : name(name) {};
	void SetName(string newName);
	virtual void Voice() = 0;
};

