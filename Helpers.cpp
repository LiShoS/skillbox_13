#include <iostream> 

//������� �������� ����� ��� int
int SqrSum(int x, int y) 
{
	return (x + y) * (x + y);
}

//������� �������� ����� ��� float
float SqrSum(float x, float y)
{
	return (x + y) * (x + y);
}

//������� ������ ���/����� �������� �� n
void PrntEvOd(int n, bool isEven) {
	if (n == 0) {
		std::cout << "������� �������� 0!" << std::endl;
		return;
	}

	int prfx = 1;

	//����� ����� n � �������� ��� �������������� n
	if (n < 0) {
		n *= -1;
		prfx = -1;
	}

	for (int tmp = 1 + isEven; tmp < n; tmp += 2)
		std::cout << prfx*tmp << " ";

	std::cout << "\n";
}